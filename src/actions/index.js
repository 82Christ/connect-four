export function startGame(gameRules) {
  return {
    type: 'GAME_START',
    payload: gameRules
  }
}

export function dropPiece(player, column) {
  return {
    type: 'DROP_PIECE',
    payload: {
      player,
      column
    }
  }
}

export function restartGame() {
  return {
    type: 'RESTART_GAME'
  }
}

export function resetApp(){
  return {
    type: 'RESET_APP'
  }
}