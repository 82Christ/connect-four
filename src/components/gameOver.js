import React, { Component } from 'react';
import '../css/App.css';
window.Tether = require('tether')
window.jQuery =  require('jquery');
window.$ = window.jQuery;
const $ = window.jQuery;
require('bootstrap');

class GameOver extends Component {
  render() {
    return(
        <div className="modal fade" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">GAME OVER</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                The winner is Player {this.props.winner} !!!
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  onClick={()=>{this.changeSettings()}}
                >
                  Change Game Settings
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={()=>{this.restartGame()}}
                >
                  Restart Game
                </button>
              </div>
            </div>
          </div>
        </div>
    );
  }

  changeSettings(){
    $('.modal').modal('hide');
    this.props.changeSettings();
  }

  restartGame() {
    $('.modal').modal('hide');
    this.props.restartGame();
  }

  componentDidMount(){
    $('.modal').modal('show');
  }
}

export default GameOver;
