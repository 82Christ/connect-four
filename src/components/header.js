import React, { Component } from 'react';
import '../css/App.css';

class Header extends Component {
  render() {
    return(
      <div className="header navbar navbar-light">
        CONNECT 4...
      </div>
    );
  }
}

export default Header;