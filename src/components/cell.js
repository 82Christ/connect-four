import React, { Component } from 'react';

class Cell extends Component {
  render() {
    return(
      <li className={"cell cell-" + this.props.color}>
        
      </li>
    );
  }

  getStyles(numCols) {
    let res = 100/numCols;
    return Object.assign({}, {
      width: res
    })
  }
}

export default Cell;
