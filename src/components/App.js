import React, { Component } from 'react';
// import logo from '../images/logo.svg';
import Header from '../components/header';
import PlayForm from '../containers/playForm';
import PlayGround from '../containers/playGround';
import '../css/App.css';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';


class App extends Component {
  render() {
    return(
      <div className="App">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
        <Header />
        <PlayForm />
        <PlayGround />
      </div>
    );
  }
}

export default App;
