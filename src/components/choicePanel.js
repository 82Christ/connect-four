import React, { Component } from 'react';
import '../css/App.css';

class ChoicePanel extends Component {
  render() {
    return(
      <div className="choice-panel">
        {this.printChoices()}
      </div>
    );
  }

  printChoices() {
    let numCols = this.props.numCols;
    let auxArr = [];
    let i=0;
    while(i<numCols) {
      auxArr[i] = <button
        key={i}
        value={i}
        className="btn btn-secondary column"
        onClick={(event)=>{this.chooseColumn(event.target.value)}}>
      </button>;
      i++;
    }
    return auxArr;
  }

  chooseColumn(elem) {
    this.props.dropPiece(this.props.player, elem);
  }
}

export default ChoicePanel;
