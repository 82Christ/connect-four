export default function(state={
  playerTurn: 1,
  gameOver: false,
  numCols: 7,
  numRows: 6,
  board: [
    []
  ]
  }, action) {
    switch(action.type) {
      case 'GAME_START':
        return gameStart(state, action);
      case'DROP_PIECE':
        return pieceDropped(state, action);
      case 'RESTART_GAME':
        return gameStart(state, action);
      case 'RESET_APP':
        return resetApp(state, action);
      default:
        return state;
    }

    function resetApp(state, action) {
      return Object.assign({}, state, {
        playerTurn: 1,
        gameOver: false,
        numCols: 7,
        numRows: 6,
        board: [
          []
        ]
      })
    }

    function pieceDropped(state, action) {

      const player = action.payload.player;
      const column = action.payload.column--;
      let boardAux = state.board;
      const numRows = state.board[column].length;
      let moveMade = false;
      let winMove = false;
      let columnFull = false;
      let oponent = null;
      
      if(player===1) {
        oponent = 2;
      } else {
        oponent = 1;
      }

      for(let i=0; i<numRows && !moveMade && !columnFull;i++) {
        if(boardAux[column][i]===1 || boardAux[column][i]===2) {
            if(i===0) {
              columnFull=true;
            }
            if(i!==0) {
              boardAux[column][i-1]=player;
              winMove=checkWin(boardAux, player, column, i-1);
              moveMade = true;
            }
        }

        if(i===(numRows-1) && !moveMade && !columnFull){
          if(player===1) {
            boardAux[column][i]=1;
            winMove=checkWin(boardAux, player, column, i);
          } else {
            boardAux[column][i]=2;
            winMove=checkWin(boardAux, player, column, i);
          }
          moveMade = true;
        }
      }
      if(!moveMade) {
        return Object.assign({}, state, {
          board: boardAux
        });
      } else if(!winMove){
        return Object.assign({}, state, {
          board: boardAux,
          playerTurn: oponent
        })
      } else {
        return Object.assign({}, state, {
          board: boardAux,
          gameOver: true
        });
      }
    }

    function checkWin(board, player, column, row){
      const checkS = checkSouth(board[column], player, row);
      const checkH = checkHorizontal(board, player, column, row);
      const checkSENW = checkSouthENorthW(board, player, column, row);
      const checkSWNE = checkSouthWNorthE(board, player, column, row);
      // if(checkS || checkH || checkSENW || checkSWNE) {
      //   alert('WWIIIIIINNNNNN');
      // }
      return checkS || checkH || checkSENW || checkSWNE;
    }

    function checkSouth(column, player, row){
      let connectedCounter = 0;
      let opponentPiece = false;
      for(let i=row; i<column.length && !opponentPiece; i++) {
        if(column[i]===player) {
          connectedCounter++;
        } else {
          opponentPiece = true;
        }
      }
      if(connectedCounter>=4) {
        return true;
      } else {
        return false;
      }
    }

    function checkHorizontal(board, player, column, row){
      let connectedCounter = 0;
      let opponentPiece = false
      for(let i=column+1; i<board.length && !opponentPiece; i++) {// CHECK RIGHT
        if(board[i][row]===player) {
          connectedCounter++;
        } else {
          opponentPiece = true;
        }
      }

      opponentPiece=false;
      for(let j=column; j>=0 && !opponentPiece; j--) {// CHECK LEFT
        if(board[j][row]===player) {
          connectedCounter++;
        } else {
          opponentPiece = true;
        }
      }
      if(connectedCounter>=4) {
        return true;
      } else {
        return false;
      }

    }

    function checkSouthENorthW(board, player, column, row) {
      let connectedCounter = 0;
      let opponentPiece = false;
      let endBoard = false;
      let i=0;
      while(!opponentPiece && !endBoard) { // CHECK SOUTHEAST
        if(board[column-i] && board[column-i][row+i]) {
          if(board[column-i][row+i]===player) {
            connectedCounter++;
          } else {
            opponentPiece = true;
          }
        } else {
          endBoard = true;
        }
        i++;
      }

      i=1;
      opponentPiece = false;
      endBoard = false;
      while(!opponentPiece && !endBoard) { // CHECK NORTHWEST
        if(board[column+i] && board[column+i][row-i]) {
          if(board[column+i][row-i]===player) {
            connectedCounter++;
          } else {
            opponentPiece = true;
          }
        } else {
          endBoard = true;
        }
        i++;
      }
      if(connectedCounter>=4) {
        return true;
      } else {
        return false;
      }

    }

    function checkSouthWNorthE(board, player, column, row) {
      let connectedCounter = 0;
      let opponentPiece = false;
      let endBoard = false;
      let i=0;
      
      while(!opponentPiece && !endBoard) { // CHECK SOUTHWEST
        if(board[column+i] && board[column+i][row-i]) {
          if(board[column+i][row-i]===player) {
            connectedCounter++;
          } else {
            opponentPiece = true;
          }
        } else {
          endBoard = true;
        }
        i++;
      }

      i=1;
      opponentPiece = false;
      endBoard = false;
      while(!opponentPiece && !endBoard) { // CHECK NORTHWEST
        if(board[column-i] && board[column-i][row-i]) {
          if(board[column-i][row-i]===player) {
            connectedCounter++;
          } else {
            opponentPiece = true;
          }
        } else {
          endBoard = true;
        }
        i++;
      }
      if(connectedCounter>=4) {
        return true;
      } else {
        return false;
      }

    }

    function gameStart(state, action)  {
      let numCols = null;
      let numRows = null;
      if(action.payload) {
        numCols = action.payload.numCols;
        numRows = action.payload.numRows;
      } else {
        numCols = state.numCols;
        numRows = state.numRows;
      }

      const board = initBoard(numCols, numRows);

      return Object.assign( {}, state, {
        numCols: numCols,
        numRows: numRows,
        board: board,
        gameOver: false
      });
    }

    function initBoard(numCols, numRows) {
      let board = [];
      for( let y=0; y < numCols; y++) {
        board[y] = [];
        for(let x=0; x < numRows; x++) {
          board[y][x] = 0;
        }
      }
      return board;
    }
}