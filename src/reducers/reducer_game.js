export default function(state={
  playAgainstPlayer: false,
  gameStarted: false,
  turn: 0, //number of turns of the game, each 2 moves (one by each player) = 1 turn
  playerTurn: 1, // 1 or 2 depending on the player
  totalMoves: 0, //incremented for each move on the game
  numRows: 6,
  numCols: 7
  }, action) {
    switch(action.type) {
      case 'GAME_START':
        return gameStart(state, action);
      case 'DROP_PIECE':
        return updateGameInfo(state, action);
      case 'RESTART_GAME':
        return restartGame(state, action);
      case 'RESET_APP':
        return resetApp(state, action);        
      default:
        return state;
    }

    function resetApp(state, action){
      return Object.assign({}, state, {
        playAgainstPlayer: false,
        gameStarted: false,
        turn: 0, 
        playerTurn: 1, 
        totalMoves: 0,
        numRows: 6,
        numCols: 7
      })
    }

    function restartGame(state, action){
      return gameStart(state, action);
    }

    function updateGameInfo(state, action){
      let totalMoves = state.totalMoves;
      if(state.playerTurn===1) {
        return Object.assign({}, state, {
          playerTurn: 2,
          totalMoves: totalMoves++,
        })
      } else {
        return Object.assign({}, state, {
          playerTurn: 1,
          totalMoves: totalMoves++,
        })
      }
    }

    function gameStart(state, action) {
      var rand = Math.random()
      var newState = {};
      let numCols = null;
      let numRows = null;
      if(action.payload) {
        numCols = action.payload.numCols;
        numRows = action.payload.numRows;
      } else {
        numCols = state.numCols;
        numRows = state.numRows;
      }
      if(rand > 0.5) {
        console.log(rand);
        newState = Object.assign({}, state, {
          playerTurn: 2,
          gameStarted: true,
          numCols: numCols,
          numRows: numRows,
          totalMoves: 0,
          turn: 0
        })
      } else {
        newState = Object.assign({}, state, {
          playerTurn: 1,
          gameStarted: true,
          numCols: numCols,
          numRows: numRows,
          totalMoves: 0,
          turn: 0
        })
      }

      return newState;
    }
}