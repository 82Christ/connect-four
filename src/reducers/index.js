import { combineReducers } from 'redux';
import GameReducer from './reducer_game';
import BoardReducer from './reducer_board';
// import ActiveProdReducer from './reducer_active_prod';
// import PaginationReducer from './reducer_pagination';
// import WishListReducer from './reducer_wishlist';
// import cartReducer from './reducer_cart';


const rootReducer = combineReducers({
  gameStatus: GameReducer,
  board: BoardReducer,
  // activeProd: ActiveProdReducer,
  // pagination: PaginationReducer,
  // wishList: WishListReducer,
  // cart: cartReducer
});

export default rootReducer;
