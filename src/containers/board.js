import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import Cell from '../components/cell';
import '../css/board.css';


class Board extends Component {
  render() {
    return (
      <div className="board-container">
          {this.printBoard()}
      </div>
    );
  }

  printBoard() {
    var board = this.props.board.board;
  
    let boardToPrint = board.map((column, index) => {
      let columnToPrint = column.map((cell, index) => {
        return <Cell key={index} color={cell} />;
      })
      return <ul key={index} className="column"> {columnToPrint} </ul>;
    })
    return boardToPrint;
  }

  renderForm(){
    return(
      <div className="play-form">
        <div className="gametype-selector">Want to play against:</div>
        <select>
          <option value="hooman">Human</option>
          <option value="ai">AI</option>
        </select>
        <button onClick={this.props.gameStart}>START GAME</button>
      </div>
    );

  }
}

function mapStateToProps(state) {
  return {
    gameStatus: state.gameStatus,
    board: state.board,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Board);
