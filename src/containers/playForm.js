import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { startGame } from '../actions/index';
import $ from 'jquery';


class PlayForm extends Component {
  render() {
    if(this.props.gameStatus.gameStarted) {
      return null;
    } else {
      let form = this.renderForm();
      return form;
    }
  }

  printChoices(type){
    let minChoice=5;
    let maxChoice=0;
    let choices = [];

    if(type==='column') {
      maxChoice=14;
    } else {
      maxChoice=20;
    }
    for(let i=minChoice; i<maxChoice; i++) {
      choices.push(<option value={i} key={i}>{i}</option>);
    }
    return choices;
  }
  
  componentDidMount() {
  }

  renderForm(){
    return(
      <form className="play-form card" onSubmit={()=>{this.startGame(event)}}>
        <div className="form-group">
          <label htmlFor="columns">Select the number of Columns</label>
          <br/>
          <select id="columns" defaultValue="7">
            {this.printChoices('column')}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="rows">Select the number of Rows</label>
          <br/>
          <select id="rows" defaultValue="6">
            {this.printChoices('row')}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="play-versus">Want to play against:</label>
          <br/>
          <select id="play-versus">
            <option value="hooman">Hooman</option>
            <option value="ai">AI</option>
          </select>
        </div>
        <button
          className="btn btn-primary"
          type="submit"
        >
          START GAME
        </button>
      </form>
    );

  }

  startGame(event) {
    event.preventDefault();

    const columns = $('#columns').val();
    const rows = $('#rows').val();
    const playVersus = $('#play-versus').val();

    let gameRules = {
      numCols: columns,
      numRows: rows,
      playVersus: playVersus
    }
    this.props.gameStart(gameRules);
  }
}


function mapStateToProps(state) {
  return {
    gameStatus: state.gameStatus,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    gameStart: startGame,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayForm);
