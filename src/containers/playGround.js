import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import Board from '../containers/board';
import ChoicePanel from '../components/choicePanel';
import GameOver from '../components/gameOver';
import { dropPiece, restartGame, resetApp } from '../actions/index';


class PlayGround extends Component {
  render() {
    return (
      <div className="play-ground">
        {this.printChoicePannel()}
        {this.printBoard()}
        {this.printPlayerTurn()}
        {this.printGameOver()}
      </div>
    );
  }

  printPlayerTurn() {
    if(this.props.gameStatus.gameStarted && !this.props.board.gameOver) {
      return (
        <div className={this.props.board.playerTurn===1 ? "jumbotron jumbo-play-1": "jumbotron jumbo-play-2"}>
          Player {this.props.board.playerTurn}
        </div>
      );
    } else {
      return null;
    }
  }
  
  printChoicePannel() {
    if(this.props.gameStatus.gameStarted && !this.props.board.gameOver) {
      return (
        <ChoicePanel
            numCols={this.props.gameStatus.numCols}
            dropPiece={this.props.dropPiece}
            player={this.props.board.playerTurn}
          />
      );
    } else {
      return null;
    }
  }

  printBoard(){
    if(this.props.gameStatus.gameStarted && !this.props.board.gameOver) {
      return <Board />
    } else {
      return null;
    }
  }
  printGameOver() {
    if(this.props.board.gameOver) {
      return (
        <div className="game-over play-ground">
          <GameOver
            winner={this.props.board.playerTurn}
            restartGame={this.props.restartGame}
            changeSettings={this.props.resetApp}
          />
          <Board />
        </div>
        )
    } else {
      return null;
    }
  }
}

function mapStateToProps(state) {
  return {
    gameStatus: state.gameStatus,
    board: state.board
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    dropPiece: dropPiece,
    restartGame: restartGame,
    resetApp: resetApp
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayGround);

